local mapa = {}
local tilesetImage
local tileQuads = {}
local tileSize = 64
local cloud_pos = {}
local c = -100
local cloud
local fg
local i = 1
local ovo = {}
local ovo_frame = 1
local ovo_pos = 360
local obs = {}
local time_scr = 0
local ctl = 5
local ct = 1
local cd = 0
local logo
local dead = {}
local ent = 0
local inm1 = {
  inm_typ = 0,
  inm_img,
  inm_pos = 1200,
  inm_e = 0
}
local inm2 = {
  inm_typ = 0,
  inm_img,
  inm_pos = 1200,
  inm_e = 0
}
local inm3 = {
  inm_typ = 0,
  inm_img,
  inm_pos = 1200,
  inm_e = 0
}

function LoadTiles(filename, nx, ny)
  tilesetImage = love.graphics.newImage(filename)
  local count = 1
  for i = 0, nx, 1 do
    for j = 0, ny, 1 do
      tileQuads[count] = love.graphics.newQuad(i * tileSize ,
      j * tileSize, tileSize, tileSize,
      tilesetImage:getWidth(),
      tilesetImage:getHeight())

      count = count + 1
    end
  end
end

function LoadMap(filename)
  local file = io.open(filename)
  local i = 1
  for line in file:lines() do
    mapa[i] = {}
    for j=1, #line, 1 do
      mapa[i][j] = line:sub(j,j)
    end
    i = i + 1
  end
  file:close()
end

function love.load()

  love.window.setFullscreen(true, "exclusive")
  for x = 1,3,1 do
    ovo[x] = love.graphics.newImage("ovo-" ..x.. ".png")
  end

  for x = 1,3,1 do
    dead[x] = love.graphics.newImage("d0" ..x.. ".png")
  end

  for o = 1,3,1 do
    obs[o] = love.graphics.newImage("obs0" ..o.. ".png")
  end

  cloud = love.graphics.newImage("Cloud.png")
  fg = love.graphics.newImage("FG.png")
  logo = love.graphics.newImage("logo.png")
  creds = love.graphics.newImage("creds.png")

  for n = 1,10, 1 do
    cloud_pos[n] = love.math.random(0, 800)
  end

  LoadMap("plataform_map - 1.txt")
  LoadTiles("plataform_tileset.png", 2, 2)
  love.graphics.setBackgroundColor(152,209,250)

  som = love.audio.newSource("4481-thatched-villagers-by-kevin-macleod.mp3", "static")

end

function love.update(dt)

  if inm1.inm_pos > 90 and inm1.inm_pos < 110 then
    if ovo_pos == 360 then
      ent = 2
      logo = dead[inm1.inm_typ]
      cd = cd + 1
    end
  end

  if inm2.inm_pos > 90 and inm2.inm_pos < 110 then
    if ovo_pos == 360 then
      ent = 2
      logo = dead[inm2.inm_typ]
      cd = cd + 1
    end
  end

  if inm3.inm_pos > 90 and inm3.inm_pos < 110 then
    if ovo_pos == 360 then
      ent = 2
      logo = dead[inm3.inm_typ]
      cd = cd + 1
    end
  end

  if cd > 0 then
    cd = cd + 1
    inm1.inm_pos = 1200
    inm1.inm_typ = 0
    inm1.inm_e = 0
    inm2.inm_pos = 1200
    inm2.inm_typ = 0
    inm2.inm_e = 0
    inm3.inm_pos = 1200
    inm3.inm_typ = 0
    inm3.inm_e = 0
    if cd == 300 then
      ent = 0
      logo = love.graphics.newImage("logo.png")
    end
  end

  if love.keyboard.isDown("space") then
    ent = 3
  end

  if love.keyboard.isDown("return") then
    ent = 1
    i = 1
    ovo_frame = 1
    ovo_pos = 360
    time_scr = 0
    ctl = 5
    ct = 1
    cd = 0
    inm1.inm_pos = 1200
    inm1.inm_typ = 0
    inm1.inm_e = 0
    inm2.inm_pos = 1200
    inm2.inm_typ = 0
    inm2.inm_e = 0
    inm3.inm_pos = 1200
    inm3.inm_typ = 0
    inm3.inm_e = 0
  end

  if ent == 1 then
    time_scr = time_scr + 1
    if love.keyboard.isDown("up") and ctl <= 35 then
      ovo_pos = 270
      ctl = ctl + 1
      if ct == 10 then
        ctl = 1
      end
      ct = ct + 1
      if ct == 1000 then
        ct = 1
      end
    else
      ovo_pos = 360
      ctl = 1
    end

    if i == 0 then
      LoadMap("plataform_map - 1.txt")
      ovo_frame = ovo_frame + 1
    elseif i == 15 then
      LoadMap("plataform_map - 2.txt")
      ovo_frame = ovo_frame + 1
    elseif i == 30 then
      LoadMap("plataform_map - 3.txt")
      ovo_frame = ovo_frame + 1
    elseif i == 45 then
      LoadMap("plataform_map - 4.txt")
      ovo_frame = ovo_frame + 1
    elseif i == 60 then
      LoadMap("plataform_map - 5.txt")
      ovo_frame = ovo_frame + 1
    elseif i == 75 then
      LoadMap("plataform_map - 6.txt")
      ovo_frame = ovo_frame + 1
    elseif i == 90 then
      LoadMap("plataform_map - 7.txt")
      ovo_frame = ovo_frame + 1
    elseif i == 105 then
      LoadMap("plataform_map - 8.txt")
      ovo_frame = ovo_frame + 1
      i = 0
    end

    if ovo_frame > 3 then
      ovo_frame = 1
    end

    i = i + 1

    for n = 1,10, 1 do
      cloud_pos[n] = cloud_pos[n] + (c * dt)
      if cloud_pos[n] < 0 then
        cloud_pos[n] = 800 + love.math.random(0.01,300)
      end
    end

    if time_scr %100 == 0 then
      if inm1.inm_3e == 0  then
        inm1.inm_3os = 800
        inm1.inm_3 = 1
        inm1.inm_typ = love.math.random(1, 3)
        inm1.inm_img = obs[inm1.inm_typ]
      elseif inm2.inm_e == 0  then
        inm2.inm_pos = 900
        inm2.inm_e = 1
        inm2.inm_typ = love.math.random(1, 3)
        inm2.inm_img = obs[inm2.inm_typ]
      elseif inm3.inm_e == 0  then
        inm3.inm_pos = 1000
        inm3.inm_e = 1
        inm3.inm_typ = love.math.random(1, 3)
        inm3.inm_img = obs[inm3.inm_typ]
      end
    end

    if time_scr %10 == 0 then
      if inm1.inm_e ==1  then
        inm1.inm_pos = inm1.inm_pos - 50
        if inm1.inm_pos < 0 then
          inm1.inm_pos = 1200
          inm1.inm_typ = 0
          inm1.inm_e = 0
        end
      end
      if inm2.inm_e ==1  then
        inm2.inm_pos = inm2.inm_pos - 50
        if inm2.inm_pos < 0 then
          inm2.inm_pos = 1200
          inm2.inm_typ = 0
          inm2.inm_e = 0
        end
      end
      if inm3.inm_e ==1  then
        inm3.inm_pos = inm3.inm_pos - 50
        if inm2.inm_pos < 0 then
          inm2.inm_pos = 1200
          inm2.inm_typ = 0
          inm2.inm_e = 0
        end
      end
    end

  end
end

function love.draw()
  if ent == 0 then
    love.audio.play(som)
    love.graphics.draw(logo, 0, 0, 0, 0.63)
    love.graphics.print("PRESSIONE ENTER PARA INICIAR", 200, 0, 0, 2, sy, ox, oy, kx, ky)
    love.graphics.print("PRESSIONE ESPAÇO PARA CRÉDITOS", 230, 30, 0, 1.5, sy, ox, oy, kx, ky)
  elseif ent == 1 then
    for i=1, 10, 1 do
      for j=1, 14, 1 do
        if (mapa[i][j] == "G") then
          love.graphics.draw(tilesetImage, tileQuads[1],
          (j * tileSize) - tileSize, (i * tileSize) - tileSize)
        elseif (mapa[i][j] == "T") then
          love.graphics.draw(tilesetImage, tileQuads[4],
          (j * tileSize) - tileSize, (i * tileSize) - tileSize)
        elseif (mapa[i][j] == "A") then
          love.graphics.draw(tilesetImage, tileQuads[7],
          (j * tileSize) - tileSize, (i * tileSize) - tileSize)
        elseif (mapa[i][j] == "P") then
          love.graphics.draw(tilesetImage, tileQuads[8],
          (j * tileSize) - tileSize, (i * tileSize) - tileSize)
        elseif (mapa[i][j] == "B") then
          love.graphics.draw(tilesetImage, tileQuads[6],
          (j * tileSize) - tileSize, (i * tileSize) - tileSize)
        elseif (mapa[i][j] == "X") then
          love.graphics.draw(tilesetImage, tileQuads[8],
          (j * tileSize) - tileSize, (i * tileSize) - tileSize)
        elseif (mapa[i][j] == "Q") then
          love.graphics.draw(tilesetImage, tileQuads[5],
          (j * tileSize) - tileSize, (i * tileSize) - tileSize)
        end
      end
    end

    --draw nuvens
    for n = 1, 7, 1 do
      love.graphics.draw(cloud, cloud_pos[n], n * 30, 0, 3)
    end

    --draw time/score
    love.graphics.print(time_scr, x, y, r, 3, 3, ox, oy, kx, ky)

    --draw personagem/obstaculos
    love.graphics.draw(ovo[ovo_frame], 100, ovo_pos, 0, 0.2)
    if inm1.inm_typ > 0 then
      if inm1.inm_typ == 1 then
        love.graphics.draw(inm1.inm_img, inm1.inm_pos, 390, r, sx, sy, ox, oy, kx, ky)
      else
        love.graphics.draw(inm1.inm_img, inm1.inm_pos, 450, r, sx, sy, ox, oy, kx, ky)
      end
    end
    if inm2.inm_typ > 0 then
      if inm2.inm_typ == 1 then
        love.graphics.draw(inm2.inm_img, inm2.inm_pos, 390, r, sx, sy, ox, oy, kx, ky)
      else
        love.graphics.draw(inm2.inm_img, inm2.inm_pos, 450, r, sx, sy, ox, oy, kx, ky)
      end
    end
    if inm3.inm_typ > 0 then
      if inm3.inm_typ == 1 then
        love.graphics.draw(inm3.inm_img, inm3.inm_pos, 390, r, sx, sy, ox, oy, kx, ky)
      else
        love.graphics.draw(inm3.inm_img, inm3.inm_pos, 450, r, sx, sy, ox, oy, kx, ky)
      end
    end
  elseif ent == 2 then
    love.audio.stop(som)
    love.graphics.draw(logo, 0, 0, 0, 0.63, sy, ox, oy, kx, ky)
    love.graphics.print("PERDEU", 300, 300, 0, 5, sy, ox, oy, kx, ky)
    love.graphics.print("PONTUAÇÂO: " ..time_scr, 320, 360, 0, 2, sy, ox, oy, kx, ky)
  elseif ent == 3 then
    love.graphics.draw(creds, 0, 0, 0, 1)
    love.graphics.print("PRESSIONE ENTER PARA INICIAR", 200, 0, 0, 2, sy, ox, oy, kx, ky)
    love.graphics.print("Music from https://filmmusic.io", 100, 100, 0, 1.5, sy, ox, oy, kx, ky)
    love.graphics.print("'Thatched Villagers' by Kevin MacLeod (https://incompetech.com)", 100, 140, 0, 1.5, sy, ox, oy, kx, ky)
    love.graphics.print("License: CC BY (http://creativecommons.org/licenses/by/4.0/)", 100, 180, 0, 1.5, sy, ox, oy, kx, ky)
  end
end
